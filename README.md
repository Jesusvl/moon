Visa to the Moon 
========================

Based on Symfony Standard Edition - a fully-functional Symfony
application.

What's inside?
--------------

All libraries included in the project are:

- [Bootstrap][1]
- [Uikit][2]
- [Parsleyjs][3]
- [Google Fonts][4]
- [Smooth Scroll][5]
- [jQuery][6]

[1]:  https://v4-alpha.getbootstrap.com/
[2]:  https://getuikit.com/
[3]:  http://parsleyjs.org/
[4]:  https://fonts.google.com/specimen/Montserrat
[5]:  https://github.com/cferdinandi/smooth-scroll
[6]: https://jquery.com/
